"""
ADES WPS-T layer for HySDS
Author: Namrata Malarout
"""
import sys
import os
import config.settings as settings
from subprocess import run
import json
from flask_ades_wpst.ades_abc import ADES_ABC
import utils.github_util as git
import utils.hysds_util as hysds
import otello
from otello import Mozart
import requests
import yaml
import time
import traceback

sys.path.append("..")

import utils.github_util as git
from utils.image_container_builder import ContainerImageBuilder

hysds_to_ogc_status = {
    "job-started": "running",
    "job-queued": "accepted",
    "job-failed": "failed",
    "job-completed": "succeeded",
    "job-revoked": "dismissed"
}


class ADES_HYSDS(ADES_ABC):
    def __init__(
        self,
        hysds_version="v4.0",
        mozart_url="https://[MOZART_IP]/mozart/api/v0.2",
        default_queue="test-job_worker-large",
        lw_queue="system-jobs-queue",
        lw_version="v0.0.5",
        grq_url="http://[GRQ_IP]/api/v0.1",
        s3_code_bucket="s3://[S3_BUCKET_NAME]",
    ):
        self._hysds_version = hysds_version
        self._mozart_url = mozart_url
        self._default_queue = default_queue
        self._lw_queue = lw_queue
        self._lw_version = lw_version
        self._grq_url = grq_url
        self.s3_code_bucket = s3_code_bucket
        m = Mozart()

    def get_procs(self):
        """
        Get all job types in HySDS
        :return:
        """
        """
        Otello Implementation
        job_types = m.get_job_types()
        for proc_name in job_types:
        """
        # For prototype,
        m = Mozart()
        job_types = m.get_job_types()
        for proc_name in job_types:
            jt = m.get_job_type(proc_name)
            jt.initialize()
            jt.describe()

    def deploy_proc(self, proc_spec):
        """
        Register a process in HySDS and add to SQLite DB
        :param proc_spec: the OGC package deployment payload
        Example proc_spec:
        process.cwl
        :return:
        """
        """
                First, clone the register-job repo from Gitlab
                The CI/CD pipeline of this repo handles the registration of the algorithm specification in HySDS.
                So we need to update the repo with the required files and push the algorithm specs of the one being registered.
        """
        try:
            repo = git.git_clone()
        except Exception as ex:
            tb = traceback.format_exc()
            error = f"Failed to create ADES required files for process deployment.\n {ex}.\n{tb}"
            raise RuntimeError(error)

        print(proc_spec)
        # Parse the request
        proc_info = proc_spec.get("$graph")[0]
        # Get process ID
        proc_id = proc_info.get("id")
        # Get process Label
        proc_label = proc_info.get("label", None)
        # Get process version
        proc_version = proc_spec.get("s:softwareVersion")
        # extract workflow inputs
        wfl_inps = proc_info.get("inputs")
        # get base docker container location
        commandline = proc_spec.get("$graph")[1]
        base_docker = commandline.get("requirements").get("DockerRequirement").get("dockerPull")

        try:
            job_spec = hysds.create_job_spec(
                wfl_inputs=wfl_inps, algorithm_docker=base_docker
            )
            hysds_io = hysds.create_hysds_io(proc_label, wfl_inps)
            repo = git.clean_up_git_repo(repo, repo_name=settings.REPO_NAME)
            hysds.write_spec_file(spec_type="hysds-io", algorithm=proc_id, body=hysds_io)
            hysds.write_spec_file(spec_type="job-spec", algorithm=proc_id, body=job_spec)
        except Exception as ex:
            tb = traceback.format_exc()
            error = f"Failed to create ADES required files for process deployment.\n {ex}.\n{tb}"
            raise RuntimeError(error)

        try:
            # creating config file
            config = hysds.create_config_file(docker_container_url=base_docker)
            hysds.write_file("{}/{}".format(settings.REPO_PATH, settings.REPO_NAME), "config.txt", config)
            # create process.cwl file
            docker_image, docker_version = hysds.get_docker_location(base_docker)
            proc_spec["$graph"][1]["requirements"]["DockerRequirement"]["dockerPull"] = \
                f"{settings.DOCKER_REGISTRY_URL}/{docker_image}:{docker_version}"
            hysds.write_file("{}/{}".format(settings.REPO_PATH, settings.REPO_NAME), "process.cwl", yaml.dump(proc_spec))
        except Exception as ex:
            tb = traceback.format_exc()
            error = "Failed to register {}\n Exception: {}\n Error: {}".format(
                f"{proc_id}:{proc_version}", ex, tb
            )
            raise RuntimeError(error)

        try:
            commit_hash = git.update_git_repo(repo, repo_name=settings.REPO_NAME,
                                              full_algorithm_name=hysds.get_algorithm_file_name(f"{proc_id}:{proc_version}"))
            print("Updated Git Repo with hash {}".format(commit_hash))
        except Exception as ex:
            tb = traceback.format_exc()
            error = "Failed to register {}. {} Traceback: {}".format(f"{proc_id}:{proc_version}", ex, tb)
            raise RuntimeError(error)

        try:
            # Check and return the pipeline info and status
            if commit_hash is None:
                raise Exception("Commit Hash can not be None.")
            gitlab_response = git.get_git_pipeline_status(project_id=settings.REGISTER_JOB_REPO_ID,
                                                          commit_hash=commit_hash)
        except Exception as ex:
            tb = traceback.format_exc()
            error = "Failed to get registration build information.\n {} Traceback: {}".format(ex, tb)
            raise RuntimeError(error)

        return gitlab_response

    def undeploy_proc(self, proc_id):
        get_jobspec_endpoint = os.path.join(
            self._MOZART_REST_API, "job_spec/type"
        )
        remove_jobspec_endpoint = os.path.join(
            self._MOZART_REST_API, "job_spec/remove"
        )
        remove_container_endpoint = os.path.join(
            self._MOZART_REST_API, "container/remove"
        )

        try:
            print(f"Getting container id information for job type job-{proc_id}")
            r = requests.get(get_jobspec_endpoint, params={"id": f"job-{proc_id}"}, verify=False)
            response = r.json()
            if response.get("success"):
                container_id = response.get("result").get("container")
                print(f"Found container {container_id} for job type {proc_id}")
            else:
                raise RuntimeError(f"Container information not found for job type {proc_id}. {r}")
        except Exception as ex:
            raise Exception(ex)
        try:
            print(f"Deleting container {container_id}")
            requests.get(remove_container_endpoint, params={"id": container_id}, verify=False)
        except Exception as ex:
            raise Exception(f"Failed to delete container {container_id}. {ex}")
        try:
            print(f"Deleting jobspec for job-{proc_id}")
            requests.get(remove_jobspec_endpoint, params={"id": f"job-{proc_id}"}, verify=False)
        except Exception as ex:
            raise Exception(f"Failed to delete jobspec job-{proc_id}. {ex}")
        return

    def exec_job(self, job_spec):
        """

        :param job_spec:
        :return:
        """
        print(job_spec)
        # Make Otello call to submit job with job type and parameters
        m = otello.Mozart()
        proc_id = f"job-{job_spec.get('proc_id')}"
        print(proc_id)
        print(job_spec.get("inputs").get("inputs"))
        job = m.get_job_type(proc_id)
        job.initialize()  # retrieving the Job wiring and parameters
        # Create params dictionary
        params = dict()
        if len(job_spec.get("inputs").get("inputs")) != 0:
            for input in job_spec.get("inputs").get("inputs"):
                params[input["id"]] = input["data"]
        job.set_input_params(params=params)
        print("Submitting job of type job-{}\n Parameters: {}".format(proc_id, params))
        try:
            hysds_job = job.submit_job(queue="verdi-job_worker", priority=0, tag="test")
            print(f"Submitted job with id {hysds_job.job_id}")
            time.sleep(2)
            return {
                "job_id": hysds_job.job_id,
                "status": hysds_job.get_status(),
                "error": None,
            }
        except Exception as ex:
            error = ex
            return {"job_id": hysds_job.job_id, "error": error}

    def dismiss_job(self, proc_id, job_id):
        # We can only dismiss jobs that were last in accepted or running state.
        # initialize job
        error = None
        job = otello.Job(job_id=job_id)
        status = job.get_status()
        print("dismiss_job got start status: ", status)
        if status in ("job-started", "job-queued"):
            # if status is started then revoke the job
            if status == "job-started":
                job.revoke()
            elif status == "job-queued":
                # if status is queued then purge (remove) the job
                job.remove()
            return {
                "job_id": job_id,
                "status": hysds_to_ogc_status.get("job-revoked"),
                "error": None,
            }
        else:
            error = f"Can not dismiss a job in {hysds_to_ogc_status.get(status)}."
            return {
                "job_id": job_id,
                "status": status,
                "error": error
            }

    def get_jobs(self, proc_id):
        jobs_result = list()
        m = otello.Mozart()
        job_set = m.get_jobs()
        print(f"filtering jobs for process job-{proc_id}")
        # {"jobID": job_id, "status": job_info["status"], "message": "Status of job {}".format(job_id)}
        for job in job_set:
            job_dets = dict()
            job_info = job.get_info()
            if job_info.get("type") == f"job-{proc_id}":
                job_dets["jobID"] = job_info.get("payload_id")
                job_dets["status"] = hysds_to_ogc_status.get(job.get_status())
                job_dets["inputs"] = (
                    job_info.get("job")
                    .get("params")
                    .get("job_specification")
                    .get("params")
                )
                jobs_result.append(job_dets)
        return jobs_result

    def get_job(self, job_spec):
        # Get PBS job status.
        #
        job_id = job_spec["jobID"]
        job = otello.Job(job_id=job_id)
        status = job.get_status()
        job_spec["status"] = hysds_to_ogc_status.get(status)
        print(f"Job status {status}")
        return job_spec

    def get_job_results(self, job_id):
        job = otello.Job(job_id=job_id)
        products = job.get_generated_products()
        print(f"Found products: {products}")
        return products
