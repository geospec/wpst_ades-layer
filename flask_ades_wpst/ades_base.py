import sys
import requests
from flask import Response
from jinja2 import Template
import logging
import yaml
import json
import hashlib
from flask_ades_wpst.sqlite_connector import sqlite_get_procs, sqlite_get_proc, sqlite_deploy_proc, \
    sqlite_undeploy_proc, sqlite_get_jobs, sqlite_get_job, sqlite_exec_job, sqlite_dismiss_job, \
    sqlite_update_job_status
from datetime import datetime

log = logging.getLogger(__name__)

class ADES_Base:

    def __init__(self, app_config):
        self.host = "http://127.0.0.1:5000"
        self._app_config = app_config
        self._platform = app_config["PLATFORM"]
        if self._platform == "Generic":
            from flask_ades_wpst.ades_generic import ADES_Generic as ADES_Platform
        elif self._platform == "K8s":
            from flask_ades_wpst.ades_k8s import ADES_K8s as ADES_Platform
        elif self._platform == "PBS":
            from flask_ades_wpst.ades_pbs import ADES_PBS as ADES_Platform
        elif self._platform == "HYSDS":
            from flask_ades_wpst.ades_hysds import ADES_HYSDS as ADES_Platform
        else:
            # Invalid platform setting.  If you do implement a new
            # platform here, you must also add it to the valid_platforms
            # tuple default argument to the flask_wpst function in
            # flask_wpst.py.
            raise ValueError("Platform {} not implemented.".\
                             format(self._platform))
        self._ades = ADES_Platform()
        
    def proc_dict(self, proc):
        return {"id": proc[0],
                "title": proc[1],
                "abstract": proc[2],
                "keywords": proc[3],
                "owsContextURL": proc[4],
                "processVersion": proc[5],
                "jobControlOptions": proc[6].split(','),
                "outputTransmission": proc[7].split(','),
                "executionUnit": proc[8]}
    
    def get_procs(self):
        saved_procs = sqlite_get_procs()
        procs = [self.proc_dict(saved_proc) for saved_proc in saved_procs]
        return procs
    
    def get_proc(self, proc_id):
        proc_desc = sqlite_get_proc(proc_id)
        return self.proc_dict(proc_desc)
    
    def deploy_proc(self, req_proc):
        """
        DONE
        :param proc_desc:
        :return:
        """
        print(req_proc)
        process_cwl = yaml.safe_load(req_proc)
        proc_info = process_cwl.get("$graph")[0]
        proc_desc = proc_info["doc"]
        proc_id = proc_info['id']
        proc_title = proc_info['label']
        proc_abstract = proc_info['doc']
        proc_keywords = []
        proc_inputs = proc_info["inputs"]
        proc_version = process_cwl["s:softwareVersion"]
        job_control = None # TODO: Figure out
        proc_desc_url = "{}/processes/{}".format(self.host, f"{proc_id}:{proc_version}")

        # creating response
        proc_summ = dict()
        proc_summ['id'] = proc_id
        proc_summ['title'] = proc_title
        proc_summ['abstract'] = proc_abstract
        proc_summ['keywords'] = proc_keywords
        proc_summ['version'] = proc_version
        proc_summ['inputs'] = proc_inputs
        proc_summ['jobControlOptions'] = job_control
        proc_summ['processDescriptionURL'] = proc_desc_url

        try:
            build_info = self._ades.deploy_proc(process_cwl)
            sqlite_deploy_proc(process_cwl)
            proc_summ["deploymentStatus"] = "In Progress"
            proc_summ["deploymentInfo"] = build_info
        except Exception as ex:
            print(f"Failed to create ADES required files for process deployment. {ex}")
            proc_summ["deploymentStatus"] = "Failure"
            proc_summ["deploymentInfo"] = f"Failed to create ADES required files for process deployment. {ex}"
        return proc_summ
            
    def undeploy_proc(self, proc_id):
        # self._ades.undeploy_proc(proc_id)
        proc_desc = self.proc_dict(sqlite_undeploy_proc(proc_id))
        print("proc_desc: ", proc_desc)
        return proc_desc

    def get_jobs(self, proc_id=None):
        # Removing sqlite query
        # jobs = sqlite_get_jobs(proc_id)
        # Query ADES
        jobs_list = self._ades.get_jobs(proc_id)
        return jobs_list

    def get_job(self, proc_id, job_id):
        # Required fields in job_info response dict:
        #   jobID (str)
        #   status (str) in ["accepted" | "running" | "succeeded" | "failed"]
        # Optional fields:
        #   expirationDate (dateTime)
        #   estimatedCompletion (dateTime)
        #   nextPoll (dateTime)
        #   percentCompleted (int) in range [0, 100]
        # job_spec = sqlite_get_job(job_id)
        # if job was dismissed, then bypass querying the ADES backend
        # job_info = {"jobID": job_id, "status": job_spec["status"]}
        # if job_spec["status"] == "dismissed":
        #     return job_info
        # otherwise, query the ADES backend for the current status
        job_spec = dict()
        job_spec["jobID"] = job_id
        ades_resp = self._ades.get_job(job_spec)
        print(ades_resp)
        job_info = dict()
        job_info["status"] = ades_resp["status"]
        job_info = {"jobID": job_id, "status": job_info["status"], "message": "Status of job {}".format(job_id)}
        # and update the db with that status
        #(job_id, job_info["status"])
        return job_info

    def exec_job(self, proc_id, job_inputs):
        """
        Execute algorithm
        :param proc_id: algorithm identifier
        :param job_inputs: Parameters for the job
        :return:
        """
        now = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%f")
        # TODO: this needs to be globally unique despite underlying processing cluster
        # job_id = f"{proc_id}-{hashlib.sha1((json.dumps(job_inputs, sort_keys=True) + now).encode()).hexdigest()}"
        job_spec = {
            "proc_id": proc_id,
            #"process": self.get_proc(proc_id),
            "inputs": job_inputs
        }
        ades_resp = self._ades.exec_job(job_spec)
        job_id = ades_resp.get("job_id")
        # ades_resp will return platform specific information that should be 
        # kept in the database with the job ID record
        sqlite_exec_job(proc_id, job_id, job_inputs, ades_resp)
        return {"code": 201, "location": "{}/processes/{}/jobs/{}".format(self.host, proc_id, job_id)}
            
    def dismiss_job(self, proc_id, job_id):
        """
        Stop / Revoke Job
        :param proc_id:
        :param job_id:
        :return:
        """
        ades_resp = self._ades.dismiss_job(proc_id, job_id)
        if ades_resp.get("error") is None:
            job_spec = sqlite_dismiss_job(job_id)
        else:
            job_spec = {"error": ades_resp}
        return job_spec

    def get_job_results(self, proc_id, job_id):
        # job_spec = self.get_job(proc_id, job_id)
        products = self._ades.get_job_results(job_id=job_id)
        job_result = dict()
        outputs = list()
        for product in products:
            id = product.get("id")
            location = None
            locations = product.get("browse_urls")
            for loc in locations:
                if loc.startswith("s3://"):
                    location = loc
            # create output blocks and append
            output = {
                "mimeType": "tbd",
                "href": location,
                "id": id
            }
            outputs.append(output)
        job_result["outputs"] = outputs
        return job_result
