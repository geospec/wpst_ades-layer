import json
import logging
import os
import re
import requests
import config.settings as settings
import time
import copy

log = logging.getLogger(__name__)

def get_mozart_job_info(job_id):
    params = dict()
    params["id"] = job_id
    session = requests.Session()
    session.verify = False
    mozart_response = session.get("{}/job/info".format(settings.MOZART_URL), params=params).json()
    return mozart_response

def get_es_query_by_job_id(job_id):
    """
    ES query for specific job ID
    :param job_id:
    :return:
    """
    query = {
     "query": {
              "bool": {
                "must": [
                    {
                      "term": {
                        "_id": job_id
                      }
                    }
                  ]
                }
              }
            }
    return query


def poll_for_completion(job_id):
    poll = True
    while poll:
        sleep_time = 2
        status_response = mozart_job_status(job_id=job_id)
        logging.info("response: {}".format(json.dumps(status_response)))
        job_status = status_response.get("status")
        if job_status != "job-queued" and job_status != "job-started":
            logging.info("Purge Job Done")
            logging.info("status: {}".format(job_status))
            logging.info("response: {}".format(status_response))
            return job_id, status_response
        else:
            if job_status == "job-queued":
                sleep_time *= 2
            else:
                # if job has started then poll more frequently
                # setting it to 2 seconds
                sleep_time = 2
            time.sleep(sleep_time)


def get_algorithm_file_name(algorithm_name):
    """
    This strips any whitespaces from the algorithm name
    :param algorithm_name:
    :return:
    """
    pattern = re.compile(r"\s+")
    string_without_whitespace = pattern.sub("", algorithm_name)
    return string_without_whitespace


def write_file(path, file_name, body):
    """
    Writes contents to a file in the cloned git repo
    :param path:
    :param file_name:
    :param body:
    :return:
    """
    if not os.path.exists(path):
        print("Creating dir")
        os.makedirs(path)
    new_file = open(os.path.join(path, file_name), 'w')
    new_file.write(body)
    new_file.close()
    print(f"Created file {os.path.join(path, file_name)}")


def get_docker_location(algorithm_docker):
    docker_name = os.path.basename(algorithm_docker)
    docker_image = docker_name.split(":")[0]
    if len(docker_name.split(":")) == 1:
        docker_version = "latest"
    else:
        docker_version = docker_name.split(":")[1]
    return docker_image, docker_version


def set_hysds_io_type(data_type):
    # TODO: Later update to support CWL data formats
    if data_type is not None:
        if data_type in ["text", "number", " datetime", "date", "boolean", "enum", "email",
                                      "textarea", "region", "passthrough", "object"]:
            return data_type
        else:
            return "text"
    else:
        return "text"


def create_hysds_io(algorithm_description, inputs, submission_type="individual"):
    """
    Creates the contents of HySDS IO file
    :param algorithm_description:
    :param inputs:
    :param verified: indicated whether algorithm is EcoSML verified
    :param submission_type:
    :return:
    """
    hysds_io = dict()
    hysds_io["label"] = algorithm_description
    hysds_io["submission_type"] = submission_type
    params = list()
    for inp in inputs:
        hysds_inp = {"name": inp, "from": "submitter", "type": "text"}
        params.append(hysds_inp)
    hysds_io["params"] = params
    print(hysds_io)
    return hysds_io


def create_job_spec(wfl_inputs, algorithm_docker):
    """
    Creates the contents of the job spec file
    :param wfl_inputs:
    :param algorithm_docker:
    :return:
    """
    command = f"{settings.CWL_EXECUTION_SCRIPT}"
    recommended_queues = [settings.DEFAULT_QUEUE]
    disk_usage = "200MB"
    soft_time_limit = 43200
    time_limit = 43260
    imported_worker_files = {
        "/static-data": ["/static-data", "rw"],
        "/tmp": ["/tmp", "rw"],
    }
    docker_image, docker_version = get_docker_location(algorithm_docker)
    dependency_images = [
        {
            "container_image_name": f"{docker_image}:{docker_version}",
            "container_image_url": f"{settings.S3_CODE_BUCKET}/{docker_image}-{docker_version}.tar.gz",
            "container_mappings": {
                "$HOME/.netrc": ["/root/.netrc"],
                "$HOME/.aws": ["/root/.aws", "ro"]
            }
        }
    ]
    params = list()

    for inp in wfl_inputs:
        if wfl_inputs.get(inp).get("type") in ["Directory", "File"]:
            hysds_inp = {"name": inp, "destination": "localize"}
        else:
            hysds_inp = {"name": inp, "destination": "context"}
        params.append(hysds_inp)

    job_spec = {
        "command": command,
        "recommended-queues": recommended_queues,
        "disk_usage": disk_usage,
        "soft_time_limit": soft_time_limit,
        "time_limit": time_limit,
        "imported_worker_files": imported_worker_files,
        "dependency_images": dependency_images,
        "post": ["hysds.utils.triage"],
        "params": params,
    }
    return job_spec


def write_spec_file(spec_type, algorithm, body, repo_name=settings.REPO_NAME):
    """
    Writes the spec files to file in docker directory
    :param spec_type:
    :param algorithm:
    :param body:
    :param repo_name:
    :return:
    """
    path = "{}/{}/docker/".format(settings.REPO_PATH, repo_name)
    file_name = "{}.json.{}".format(spec_type, get_algorithm_file_name(algorithm))
    # Additions to job spec schema in HySDS core v3.0
    body.update({"soft_time_limit": 86400, "time_limit": 86400})
    write_file(path, file_name, json.dumps(body))


def write_dockerfile(repo_name, dockerfile_content):
    """
    Write the docker file to the docker directory
    :param dockerfile_content:
    :return:
    """
    path = "{}/{}/docker/".format(settings.REPO_PATH, repo_name)
    write_file(path, "Dockerfile", dockerfile_content)


def create_config_file(docker_container_url):
    """
    Creates the contents of config.txt file
    Contains the information needed for the job container

    Example content:
    BASE_IMAGE_NAME=<registery-url>/root/jupyter_image/vanilla:1.0
    REPO_URL_WITH_TOKEN=https://<gitlab-token>@mas.maap-project.org/root/dps_plot.git
    REPO_NAME=dps_plot
    BRANCH=master
    GRQ_REST_URL=<grq-ip>/api/v0.1
    MAAP_API_URL=https:api.dit.maap-project.org/api
    MOZART_URL=<mozart-ip>/mozart/api/v0.1
    S3_CODE_BUCKET=s3://s3.amazon.aws.com/<bucket-name>

    :param repo_name:
    :param repo_url_w_token:
    :param repo_branch:
    :param verified: Indicated if algorithm is EcoSML verified
    :param docker_container_url:
    :param build_command:
    :return: config.txt content
    """
    
    config_content = "BASE_IMAGE_NAME={}\n".format(docker_container_url)
    config_content += "GRQ_REST_URL={}\n".format(settings.GRQ_REST_URL)
    config_content += "MAAP_API_URL={}\n".format(settings.MAAP_API_URL)
    config_content += "MOZART_URL={}\n".format(settings.MOZART_V1_URL)
    config_content += "S3_CODE_BUCKET={}\n".format(settings.S3_CODE_BUCKET)
    return config_content


def create_dockerfile(base_docker_image_name, label, repo_url, repo_name, branch):
    """
    This will create the Dockerfile the container builder on the CI will use.
    :param base_docker_image_name:
    :param repo_url:
    :param repo_name:
    :param branch:

    sample:
    FROM ${BASE_IMAGE_NAME}

    MAINTAINER malarout "Namrata.Malarout@jpl.nasa.gov"
    LABEL description="Lightweight System Jobs"

    # provision lightweight-jobs PGE
    USER ops

    #clone in the SPDM repo
    RUN git clone ${REPO_URL_WITH_TOKEN} && \
        cd ${REPO_NAME} && \
        git checkout ${BRANCH} && \

    # set entrypoint
    WORKDIR /home/ops
    CMD ["/bin/bash", "--login"]
    :return:
    """
    dockerfile = "FROM {}\n".format(base_docker_image_name)
    dockerfile += "LABEL description={}\n".format(label)
    dockerfile += "USER ops\n"
    dockerfile += "RUN git clone {} && ".format(repo_url)
    dockerfile += "    cd {} && ".format(repo_name)
    dockerfile += "    git checkout {}".format(branch)
    dockerfile += "\n# set entrypoint\n"
    dockerfile += "WORKDIR /home/ops\n"
    dockerfile += "CMD [\"bin/bash\", \"--login\"]"

    return dockerfile


def get_job_submission_json(algorithm, branch=settings.VERSION):
    """
    This JSON is sent back by the CI, on successful container build
    :param algorithm:
    :param branch:
    :return:
    """
    job_json = dict()
    job_json["job_type"] = "job-{}:{}".format(algorithm, branch)
    return json.dumps(job_json)


def get_algorithms():
    """
    Get the list of job specs
    :return:
    """
    headers = {'content-type': 'application/json'}

    session = requests.Session()
    session.verify = False

    try:
        mozart_response = session.get("{}/job_spec/list".format(settings.MOZART_URL), headers=headers, verify=False)
    except Exception as ex:
        raise ex

    algo_list = mozart_response.json().get("result")
    maap_algo_list = list()
    for algo in algo_list:
        if not algo.startswith("job-lw-") and not algo.startswith("job-lightweight"):
            maap_algo_list.append(algo)

    return maap_algo_list


def mozart_submit_job(job_type, params={}, queue=settings.DEFAULT_QUEUE, dedup="false", identifier=["maap-api_submit"]):
    """
    Submit a job to Mozart
    :param job_type:
    :param params:
    :param queue:
    :param dedup:
    :param identifier:
    :return:
    """

    logging.info("Received parameters for job: {}".format(json.dumps(params)))

    job_payload = dict()
    job_payload["type"] = job_type
    job_payload["queue"] = queue
    job_payload["priority"] = 0
    if identifier is not None:
        if type(identifier) is dict:
            job_payload["tags"] = json.dumps(identifier)
        else:
            identifier_list = identifier.split(",")
            job_payload["tags"] = json.dumps(identifier_list)
    else:
        job_payload["tags"] = json.dumps(["maap-api_submit"])

    # assign username to job
    if params.get("username") is not None:
        job_payload["username"] = params.get("username").strip()

    # remove username from algo params if provided.
    params.pop('username', None)
    job_payload["params"] = json.dumps(params)
    job_payload["enable_dedup"] = dedup

    logging.info("job payload: {}".format(json.dumps(job_payload)))

    headers = {'content-type': 'application/json'}

    session = requests.Session()
    session.verify = False

    try:
        mozart_response = session.post("{}/job/submit".format(settings.MOZART_URL),
                                       params=job_payload, headers=headers,
                                       verify=False).json()
    except Exception as ex:
        raise ex

    job_id = mozart_response.get("result")

    try:
        # Adding check to see if job was deduped
        time.sleep(5)
        status = mozart_job_status(job_id).get("status")
        if status == "job-deduped":
            try:
                logging.info("Submitted job was deduped. Looking for original job.")
                logging.info(f"Deduped job mozart response: {json.dumps(mozart_response)}")
                job_info_response = get_mozart_job_info(job_id)
                orig_job_id = job_info_response.get("result").get("dedup_job")
                logging.info(f"Found original job id: {orig_job_id}")
                updated_mozart_response = copy.deepcopy(mozart_response)
                updated_mozart_response.update({"result": orig_job_id})
                orig_job_status = mozart_job_status(orig_job_id).get("status")
                # overwriting deduped job's mozart response with original job id.
                updated_mozart_response.update({"orig_job_status": orig_job_status})
                mozart_response = updated_mozart_response
                logging.info(f"Updated job mozart response with original job: {json.dumps(mozart_response)}")
            except Exception as ex:
                raise ex
    except Exception:
        # Tried to query mozart too soon after job submission. Just return the job submission response from mozart
        return mozart_response
    return mozart_response


def get_username_from_job_submission(params={}):
    if params.get("username") is not None:
        return params.get("username").strip()
    else:
        return None
        

def mozart_job_status(job_id):
    """
    Returns mozart's job status
    :param job_id:
    :return:
    """
    params = dict()
    params["id"] = job_id

    session = requests.Session()
    session.verify = False

    try:
        mozart_response = session.get("{}/job/status".format(settings.MOZART_URL), params=params)
        logging.info("Job Status::: {}".format(mozart_response.json()))
    except Exception as ex:
        raise ex

    return mozart_response.json()


def mozart_delete_job_type(job_type):
    params = dict()
    params["id"] = job_type

    session = requests.Session()
    session.verify = False

    try:
        mozart_response = session.get("{}/job_spec/remove".format(settings.MOZART_URL), params=params)
    except Exception as ex:
        raise ex

    return mozart_response.json()


def get_job_spec(job_type):
    """
    Get the job spec of a registered algorigthm
    :param job_type:
    :return:
    """
    headers = {'content-type': 'application/json'}

    session = requests.Session()
    session.verify = False

    try:
        mozart_response = session.get("{}/job_spec/type?id={}".format(settings.MOZART_V1_URL, job_type), headers=headers,
                                      verify=False)
    except Exception as ex:
        raise ex

    return mozart_response.json()


def get_hysds_io(hysdsio_type):
    """
        Get the hysds-io of a registered algorigthm
        :param hysdsio_type:
        :return:
        """
    headers = {'content-type': 'application/json'}

    session = requests.Session()
    session.verify = False

    try:
        grq_response = session.get("{}/hysds_io/type?id={}".format(settings.GRQ_URL, hysdsio_type), headers=headers,
                                   verify=False)
        logging.debug(grq_response)
    except Exception as ex:
        raise ex

    return grq_response.json()


def get_recommended_queue(job_type):
    response = get_job_spec(job_type)
    recommended_queues = response.get("result", None).get("recommended-queues", None)
    recommended_queue = recommended_queues[0] if type(recommended_queues) is list else None
    return recommended_queue if recommended_queue != "" else settings.DEFAULT_QUEUE


def validate_job_submit(hysds_io, user_params):
    """
    Given user's input params and the hysds-io spec for the job type
    This function validates if all the input params were provided,
    if not provided then fill in the default value specified during registration
    :param hysds_io:
    :param user_params:
    :return:
    """
    # building a dictionary of key value pairs of the parameters registered
    reg_params = hysds_io.get("result").get("params")
    known_params = dict()
    for param in reg_params:
        param_info = dict()
        param_name = param.get("name")
        param_info["from"] = param.get("from")
        param_info["default"] = param.get("default", None)
        param_info["type"] = param.get("type", str)
        known_params[param_name] = param_info

    """
    Verify if user provided all the parameters
    - if not, check if default was provided on registration and set to that value
    - else throw an error saying parameter missing
    """
    validated_params = dict()
    # do not miss the username in params
    validated_params["username"] = user_params.get("username")
    for p in known_params:
        if user_params.get(p) is not None:
            validated_params[p] = user_params.get(p)
            # TODO: Check datatype of input, if provided in spec
        else:
            if known_params.get(p).get("default") is not None:
                validated_params[p] = known_params.get(p).get("default")
            else:
                raise Exception("Parameter {} missing from inputs. Didn't find any default set for it in "
                                "algorithm specification. Please specify it and attempt to submit.".format(p))
    return validated_params


def get_mozart_job(job_id):
    job_status = mozart_job_status(job_id).get("status")
    if job_status == "job-completed" or job_status == "job-failed":
        try:
            mozart_response = get_mozart_job_info(job_id)
            result = mozart_response.get("result")
            return result
        except Exception as ex:
            raise ex
    else:
        raise Exception("Aborting retrieving information of job because status is {}".format(job_status))


def get_mozart_queues():
    session = requests.Session()
    session.verify = False

    try:
        mozart_response = session.get("{}/queue/list".format(settings.MOZART_URL)).json()
        logging.debug("Response from {}/queue/list:\n{}".format(settings.MOZART_URL, mozart_response))
        if mozart_response.get("success") is True:
            try:
                queues_list = mozart_response.get("result").get("queues")
                result = [queue for queue in queues_list if queue.startswith(settings.PROJECT_QUEUE_PREFIX)]
                return result
            except Exception as ex:
                raise ex
    except:
        raise Exception("Couldn't get list of available queues")


def get_mozart_jobs(username, page_size=10, offset=0):
    """
        Returns mozart's job list
        :param username:
        :param page_size:
        :param offset:
        :return:
        """
    params = dict()
    params["page_size"] = page_size
    params["offset"] = offset  # this is specifies the offset

    session = requests.Session()
    session.verify = False

    try:
        param_list = ""
        for key, value in params.items():
            param_list += "&{}={}".format(key, value)

        if settings.HYSDS_VERSION == "v3.0":
            if username is not None:
                param_list += f"&username={username}"
            url = "{}/job/list?{}".format(settings.MOZART_URL, param_list[1:])
        elif settings.HYSDS_VERSION == "v4.0":
            url = "{}/job/user/{}?{}".format(settings.MOZART_URL, username, param_list[1:])
        logging.info("GET request to find jobs: {}".format(url))
        mozart_response = session.get(url)

    except Exception as ex:
        raise ex

    return mozart_response.json()


def get_jobs_info(job_list):
    """
    Returns Job infos
    :param job_list:
    :return:
    """
    jobs_info = list()
    try:
        for job_id in job_list:
            job = dict()
            mozart_response = get_mozart_job_info(job_id)
            success = mozart_response.get("success")
            if success is True:
                job[job_id] = mozart_response.get("result")
            else:
                job[job_id] = {"message": "Failed to get job info"}
            jobs_info.append(job)
    except Exception as ex:
        raise ex

    return jobs_info


def delete_mozart_job_type(job_type):
    params = dict()
    params["id"] = job_type
    session = requests.Session()
    session.verify = False

    response = mozart_delete_job_type(job_type)
    status = response.get("success")
    message = response.get("message")
    if status is True:
            return status
    else:
        raise Exception("Failed to remove job spec. Error: {}".format(message))


def delete_mozart_job(job_id):
    """
    This function deletes a job from Mozart
    :param job_id:
    :return:
    """
    job_type = "job-lw-mozart-purge:{}".format(settings.HYSDS_LW_VERSION)
    params = {
        "query": get_es_query_by_job_id(job_id),
        "component": "mozart",
        "operation": "purge"
    }
    logging.info("Submitting job of type {} with params {}".format(job_type, json.dumps(params)))
    submit_response = mozart_submit_job(job_type=job_type, params=params, queue=settings.LW_QUEUE)
    lw_job_id = submit_response.get("result")
    logging.info(lw_job_id)

    # keep polling mozart until the purge job is finished.
    return poll_for_completion(lw_job_id)


def revoke_mozart_job(job_id):
    """
    This function deletes a job from Mozart
    :param job_id:
    :return:
    """
    job_type = "job-lw-mozart-revoke:{}".format(settings.HYSDS_LW_VERSION)
    params = {
        "query": get_es_query_by_job_id(job_id),
        "component": "mozart",
        "operation": "revoke"
    }
    logging.info("Submitting job of type {} with params {}".format(job_type, json.dumps(params)))
    submit_response = mozart_submit_job(job_type=job_type, params=params, queue=settings.LW_QUEUE)
    lw_job_id = submit_response.get("result")
    logging.info(lw_job_id)

    # keep polling mozart until the purge job is finished.
    return poll_for_completion(lw_job_id)


def pele_get_product_by_id(id):
    return



