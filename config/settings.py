# GitLab Settings
GIT_REPO_URL = ""
WFL_CWL_LOCATION = "/app/process.cwl"
CWL_EXECUTION_SCRIPT = "/app/dps_wrapper.sh"
REPO_NAME = 'sample-register-job'
REPO_PATH = '/wpst_ades-layer'
VERSION = 'master'
GITLAB_TOKEN = ""
GITLAB_API_TOKEN = ""
# SISTER Settings
DOCKER_REGISTRY_URL = 'localhost:5050'
MAAP_API_URL = "https://MAAP-API"
REGISTER_JOB_REPO_ID = 1
GIT_API_URL = ""
# HySDS settings
DEFAULT_QUEUE = 'test-job_worker-large'
LW_QUEUE = 'system-jobs-queue'
DISK_USAGE = "200MB"
GRQ_REST_URL = "http://[GRQ-IP]"
MOZART_V1_URL = "http://[mozart-IP]"
S3_CODE_BUCKET = "s3://[bucket-name]"


